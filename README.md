# **Introduction**
------
This is for the network time protocol installation.

# **Important**
------
Note that you should install the ntp first.

# **Usage**
------
This script should be executed by root.
If you want to edit the time server or any other restriction, please modify the ntp.conf file.
```
$ bash install
```

# **Author**

Jason Chen
